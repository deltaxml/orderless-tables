# Orderless Tables Sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DocBook-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DocBook-Compare-5_2_2_j/samples/sample-name`.*

---
## Contents of the sample files

The key part of the sample input files is the table. The first version has the rows of the table in code number order, 01 to 06. The second version has three changes: the order of the rows is shuffled to be alphabetical by product name, the row for code 06 has a price change, and a new row with code 07 has been added.

There are three pairs of input files that include differet processing instructions to control the table comparison.

1. no-PIs. These files contain no PIs and the rows in the table will be compared in an 'ordered' manner. This results in a large number of row additions and deletions because of the re-ordering in version 2.
2. with-PI. These files contain a processing instruction for dxml-orderless-rows in the table. This means that simple row re-ordering is not considered to be a change because the rows are compared in an 'orderless' manner. The result contains fewer row additions and deletions. Item 06 is still marked as a row deletion and addition because, by default, orderless items only match together when they are precisely the same. The next pair of inputs uses a 'key' instruction to improve the result.
3. with-PI-and-key. These files include a keying instruction on the processing instruction so that it now reads <?dxml-orderless-rows cell-pos:1?>. This means that the text value in the first cell entry in each row is used as a 'key' to uniquely identify the row during comparison. This means that row 06 is now marked up with the price change rather than as added and deleted.

## Running the sample via the Ant build script

If you have Ant available on your path, simply navigate to the sample directory and run the following command:

	ant

The Ant command runs three comparisons, using each of the pairs of input files and producing three result files.

If you don't have Ant installed, you can still run each sample individually from the command-line.

## Running the sample from the Command line

It is possible to compare two DocBook files using the command line tool from sample directory as follows. We will use the orderless-table-with-PI-cellpos input files as an example here. The following line can be copied and pasted directly into the Terminal window, when your current directory is orderless-tables.

    java -jar ../../deltaxml-docbook.jar compare docbook-orderless-table-version-1-with-PI-and-key.xml docbook-orderless-table-version-2-with-PI-and-key.xml result-with-PI-and-key.xml indent=yes
    
Other parameters are available as discussed in the [User Guide](https://docs.deltaxml.com/docbook-compare/latest/user-guide) and summarised by the following command

    java -jar ../../deltaxml-docbook.jar describe
   

To clean up the sample directory, run the following Ant command.

	ant clean